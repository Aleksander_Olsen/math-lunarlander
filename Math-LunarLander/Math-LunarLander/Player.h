#pragma once

#include "Globals.h"
#include "Rocket.h"
#include <SDL.h>

class Player {
private:
	glm::vec2 pos;
	glm::vec2 vel;
	glm::vec2 acc;
	glm::vec2 force;
	glm::vec2 shootDir;
	glm::vec2 mousePos;
	SDL_Rect rect;
	float fireAngle;
	float newFireAngle;
	float mass;
	float invMass;
	bool up;
	bool down;
	bool left;
	bool right;
	std::vector<std::unique_ptr<Rocket>> rockets;
	int curRocket;
public:
	Player();
	~Player();

	void wrapAround();
	void applyForce(glm::vec2 _force);

	void moveUp();
	void moveLeft();
	void moveDown();
	void moveRight();

	void calcFireAngle(float dt);
	void fireRocket();

	bool handleKeys(SDL_Event &e);
	void update(float dt);
	void render(SDL_Renderer* gRenderer);
};

