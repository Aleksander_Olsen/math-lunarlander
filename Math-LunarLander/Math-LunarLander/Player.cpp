#include "Player.h"



Player::Player() {
	pos = { SCREEN_WIDTH / 2.0, SCREEN_HEIGHT / 2.0 };
	vel = { 0.0, 0.0 };
	acc = { 0.0, 0.0 };
	mass = 1.0;
	invMass = 1.0 / mass;
	fireAngle = 0.0;
	newFireAngle = 0.0;
	curRocket = 0;
	up = false;
	left = false;
	down = false;
	right = false;
}


Player::~Player() {
	for (int i = rockets.size(); i < 0; i--) {
		rockets.pop_back();
	}
}

/*
Wrap the player around the screen on the x-axis.
Stops the player from going abow or below the screen on y-axis.
*/
void Player::wrapAround() {
	if (pos.x < -10) {
		pos.x = SCREEN_WIDTH + 10;
	}
	if (pos.x - 10 > SCREEN_WIDTH) {
		pos.x = -10;
	}

	if (pos.y < 10) {
		pos.y = 10;
		vel.y = 0.0;
	}
	if (pos.y + 10 > SCREEN_HEIGHT) {
		pos.y = SCREEN_HEIGHT - 10;
		vel.y = 0.0;
		vel.x = 0.0;
	}
}

void Player::applyForce(glm::vec2 _force) {
	force += _force;
}

void Player::moveUp() {
	applyForce(glm::vec2(0.0, -PLAYER_SPEED));
}

void Player::moveLeft() {
	applyForce(glm::vec2(-PLAYER_SPEED, 0.0));
}

void Player::moveDown() {
	applyForce(glm::vec2(0.0, PLAYER_SPEED));
}

void Player::moveRight() {
	applyForce(glm::vec2(PLAYER_SPEED, 0.0));
}

/*
Calculates the angle between the player position and mouse position.
*/
void Player::calcFireAngle(float dt) {
	float angleDiff = fireAngle - newFireAngle;
	if (glm::sign(angleDiff) * angleDiff < 5 * dt) {
		fireAngle = newFireAngle;
	} else if (glm::sign(angleDiff) * angleDiff < 3.1415) {
		fireAngle -= 5 * dt * glm::sign(angleDiff);
	} else {
		fireAngle += 5 * dt * glm::sign(angleDiff);
	}

	if (fireAngle > 3.1415) {
		fireAngle -= 2 * 3.1415;
	} else if (fireAngle < -3.1415) {
		fireAngle += 2 * 3.1415;
	}
}

/*
Either adds a new rocket to the rockets vector if not full,
or uses the oldest rocket and fire that rockect again.
*/
void Player::fireRocket() {
	glm::vec2 startPos = {pos.x, pos.y};
	glm::vec2 startVel = { glm::cos(fireAngle), glm::sin(fireAngle) };
	
	if (rockets.size() < MAX_ROCKETS) {
		rockets.push_back(std::make_unique<Rocket>(startPos, startVel));
	} else {
		rockets.at(curRocket)->fire(startPos, startVel);
		++curRocket;
	}

	if (curRocket >= MAX_ROCKETS) {
		curRocket = 0;
	}
}

bool Player::handleKeys(SDL_Event & e) {

	if (e.type == SDL_KEYDOWN && e.key.repeat == 0) {
		switch (e.key.keysym.sym) {
		case SDLK_q:
		case SDLK_ESCAPE:
			return true;
		case SDLK_w:
			up = true;
			break;
		case SDLK_a:
			left = true;
			break;
		case SDLK_s:
			down = true;
			break;
		case SDLK_d:
			right = true;
			break;
		}
	} else if (e.type == SDL_KEYUP && e.key.repeat == 0) {
		switch (e.key.keysym.sym) {
		case SDLK_q:
		case SDLK_ESCAPE:
			return true;
		case SDLK_w:
			up = false;
			break;
		case SDLK_a:
			left = false;
			break;
		case SDLK_s:
			down = false;
			break;
		case SDLK_d:
			right = false;
			break;
		}
	} else if (e.type == SDL_MOUSEMOTION) {
		int x, y;
		SDL_GetMouseState(&x, &y);
		mousePos = { x, y };
		shootDir = mousePos - pos;
		newFireAngle = atan2f(shootDir.y, shootDir.x);
	} else if (e.type == SDL_MOUSEBUTTONDOWN) {
		fireRocket();
	}

	return false;
}

void Player::update(float dt) {
	wrapAround();

	//Apply force to different dirrections.
	if (up) {
		moveUp();
	} 
	if (down) {
		moveDown();
	}
	if (left) {
		moveLeft();
	}
	if (right) {
		moveRight();
	}

	//Apply gravity force.
	applyForce(glm::vec2(0.0, GRAVITY));
	//Apply friction force.
	applyForce(-vel * (float)0.5 * (float)0.5);
	
	//Calculate acceleration, velocity and position.
	acc = force * invMass;
	vel += acc * dt;
	pos += vel * dt;

	//Reset force.
	force = { 0.0 ,0.0 };

	//If velocity is small enough, stop completely.
	if (glm::length(vel.x) < 0.05) {
		vel.x = 0.0;
	}
	if (glm::length(vel.y) < 0.02) {
		vel.y = 0.0;
	}
	
	//Stop velocity from going above max speed.
	vel = glm::clamp(vel, -MAX_SPEED, MAX_SPEED);

	//Calculate angle to fire rocket at.
	calcFireAngle(dt);

	//Update all rockets.
	for (auto &x : rockets) {
		//Only update if inside screen.
		if (x->inView()) {
			x->update(dt);
		}
	}
}

void Player::render(SDL_Renderer * gRenderer) {
	rect = { (int)pos.x - 10,(int)pos.y - 10, 20, 20 };
	SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderFillRect( gRenderer, &rect );

	//Render all rockets.
	for (auto &x : rockets) {
		//Only render if inside screen.
		if (x->inView()) {
			x->render(gRenderer);
		}
	}
}
