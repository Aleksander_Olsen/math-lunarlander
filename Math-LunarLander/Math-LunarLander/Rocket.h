#pragma once

#include "Globals.h"
#include <SDL.h>

class Rocket {
private:
	float t;
	glm::vec2 pos;
	glm::vec2 vel;
	SDL_Rect rect;
public:
	Rocket();
	Rocket(glm::vec2 startPos, glm::vec2 startVel);
	~Rocket();

	void fire(glm::vec2 startPos, glm::vec2 startVel);
	bool inView();

	void update(float dt);
	void render(SDL_Renderer* gRenderer);
};

