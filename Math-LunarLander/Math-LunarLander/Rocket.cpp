#include "Rocket.h"



Rocket::Rocket()
{
}

Rocket::Rocket(glm::vec2 startPos, glm::vec2 startVel) {
	fire(startPos, startVel);
}


Rocket::~Rocket()
{
}

/*
Start rocket at player postion.
Set velocity to the direction of mouse position and to max speed.
*/
void Rocket::fire(glm::vec2 startPos, glm::vec2 startVel) {
	pos = startPos;
	vel = startVel * MAX_ROCKET_SPEED;
}

/*
Return true if rocket is inside screen area.
*/
bool Rocket::inView() {
	if (pos.x > 0.0 && pos.x < SCREEN_WIDTH) {
		if (pos.y > 0.0 && pos.y < SCREEN_HEIGHT) {
			return true;
		}
	}

	return false;
}

void Rocket::update(float dt) {
	pos += vel * dt;
}

void Rocket::render(SDL_Renderer * gRenderer) {
	rect = { (int)pos.x - 3,(int)pos.y - 3, 6, 6 };
	SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderFillRect(gRenderer, &rect);
}
