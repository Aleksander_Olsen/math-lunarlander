#pragma once

#include <gl\glew.h>
#include <gl\glu.h>


#define GLM_FORCE_RADIANS //Using degrees with glm is deprecated.
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <vector>
#include <iostream>
#include <memory>
#include <cmath>


//Screen dimension constants
const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 960;

const float MAX_SPEED = 400.0;
const float GRAVITY = 80.0;
const float PLAYER_SPEED = 150.0;

const int MAX_ROCKETS = 100;
const float MAX_ROCKET_SPEED = 500;
